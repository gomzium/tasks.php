<?php

function rewriteJsonFile(string $pathToJsonFile, string $key, $value): void
{
    // JSON файл лежит по пути $pathToJsonFile. Необходимо получить содержимое этого файла,
    // добавить в него поле $key со значением $value и перезаписать.
    $jsonStr = file_get_contents($pathToJsonFile);
    $jsonArray = json_decode($jsonStr,  TRUE);
    $jsonArray[$key] = $value;
    file_put_contents($pathToJsonFile, json_encode($jsonArray));
}
